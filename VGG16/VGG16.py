"""
VGG16 model training using PyTorch

usage:
        python VGG16.py [dataset path] [cuda/cpu]
"""

# import libraries
import time
import torch
import numpy as np
import seaborn as sns
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from torchvision import transforms
import torchvision.models as models
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader
from sys import argv
import os 

# global parameters
STORE_PROGRESS_GRAPH = True
BATCH_SIZE = 128
EPOCH = 20
DEVICE = torch.device(argv[2] if torch.cuda.is_available() else "cpu")
FILE = "VGG"+str(time.time())

# sim folder creation
try: 
    os.mkdir(FILE)
except OSError as error: 
    print(error) 

# usefull functions
def progessGraphs(train_loss,train_acc):
    ''' Generate and store loss & accuracy graph progress 
    
    Parameters:
        train_loss (list): network loss for each epoch
        train_acc (list): network accuracy for each epoch
    '''
    
    epochs = [i for i in range(len(train_loss))]
    fig, ax1 = plt.subplots()

    color = 'tab:blue'

    ax1.set_xlabel('Epoch')
    ax1.set_ylabel('train_acc (%)', color=color)
    ax1.plot(epochs, train_acc, label="train_acc", color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_ylim([0, 1])

    ax2 = ax1.twinx()

    color = 'tab:red'
    ax2.set_ylabel('train_loss', color=color) 
    ax2.plot(epochs, train_loss, label="train_loss", color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim([0, max(train_loss)])

    fig.legend()
    fig.tight_layout()
    plt.savefig(FILE+"/Graphs.svg")

def train_model(model, criterion, optimizer, num_epochs=25):
    '''
    Function to train the model

    Parameters:
        model: the VGG16 model
        criterion: loss function
        optimizer: the used optimizer
        num_epochs (int): number of epochs
    Returns:
        model: trained model
        final_acc: accuracy
        final_loss: loss value
    '''

    since = time.time()
    final_loss = []
    final_acc = []
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)
        # Each epoch has a training and validation phase
        for phase in ['train']:
            if phase == 'train':
                model.train()  # Set model to training mode
                data_size = len(train_loader.sampler)
            else:
                model.eval()   # Set model to evaluate mode
                data_size = len(test_loader.sampler)
            running_loss = 0.0
            running_corrects = 0
            # Iterate over data.
            for inputs, labels in train_loader if phase == 'train' else test_loader:
                inputs = inputs.to(DEVICE)
                labels = labels.to(DEVICE)

                # zero the parameter gradients
                optimizer.zero_grad()
                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)
                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            epoch_loss = running_loss / data_size
            epoch_acc = running_corrects.double() / data_size
            final_acc.append(epoch_acc.cpu())
            final_loss.append(epoch_loss)

            print('Loss: {:.4f} Acc: {:.4f}'.format(epoch_loss, epoch_acc))

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    return model, final_acc, final_loss

def test_model(model, criterion, optimizer):
    '''
    Function to test the model performance

    Parameters:
        model: the VGG16 model
        criterion: loss function
        optimizer: the used optimizer
    Returns:
        loss: loss value
        acc: accuracy
        matrix: confusion matrix
    '''

    matrix = np.zeros((len(class_to_idx),len(class_to_idx)))
    torch.set_grad_enabled(False)
    model.eval()
    running_loss = 0.0
    running_corrects = 0
    Targets = []
    Outputs = []
    # Iterate over data.
    for inputs, labels in test_loader:
        inputs = inputs.to(DEVICE)
        labels = labels.to(DEVICE)

        # zero the parameter gradients
        optimizer.zero_grad()
        # forward
        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        loss = criterion(outputs, labels)
        # statistics
        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)
        Targets.append(labels.data.cpu())
        Outputs.append(preds.cpu())

        for x,y in zip(preds,labels.data):
            matrix[x][y] = matrix[x][y] + 1

    loss = running_loss / len(test_loader.sampler)
    acc = running_corrects.double() / len(test_loader.sampler)

    Outputs = np.concatenate(Outputs)
    Targets = np.concatenate(Targets)

    return loss, acc, matrix

####################################################################

# model definition
model_ft = models.vgg16(weights=models.VGG16_Weights.DEFAULT)

# data transformation
transform =   transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor()
])

# getting Train and Test set from folders
train = ImageFolder(str(argv[1])+"/Train",transform=transform)
test = ImageFolder(str(argv[1])+"/Test",transform=transform)
class_to_idx = train.class_to_idx

# dataloaders
train_loader = DataLoader(
    train, batch_size=BATCH_SIZE, shuffle=True,
    num_workers=4, pin_memory=True
)
test_loader = DataLoader(
    test, batch_size=BATCH_SIZE, shuffle=False,
    num_workers=4, pin_memory=True
)

# update the last layer of VGG16 for training
num_ftrs = model_ft.classifier[-1].in_features
model_ft.classifier[-1] = nn.Linear(num_ftrs, len(class_to_idx))

# set optimizer & loss function
criterion = nn.CrossEntropyLoss()
optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
    
# Fix all layers except last one before training
for layer in model_ft.features.parameters():
    layer.requires_grad = False
for layer in model_ft.classifier[:-1].parameters():
    layer.requires_grad = False

if torch.cuda.is_available():
    model_ft.cuda(argv[2])

# Training the model
print("-" * 3,"Training","-" * 3)
model_ft, final_acc, final_loss = train_model(model_ft, criterion, optimizer_ft, num_epochs=EPOCH)

# Testing the model
print("-" * 3,"Testing","-" * 3)
test_loss, test_acc, matrix = test_model(model_ft,criterion, optimizer_ft) # F1_score, Recall_score, Precision_score,
print("test Loss: {:.4f} Acc: {:.4f}".format(test_loss,test_acc))

# store the train test information in a file
f = open(f"{FILE}/model_perf", "w")
f.write('train Loss: {:.4f} Acc: {:.4f}'.format(final_loss[-1], final_acc[-1])+"\n")
f.write('test Loss: {:.4f} Acc: {:.4f}'.format(test_loss, test_acc)+"\n")
f.write(str(class_to_idx))
f.close()

# Store progress of loss and accuracy during training
if STORE_PROGRESS_GRAPH:
    progessGraphs(final_loss,final_acc)

# Store Confusion matrix
plt.figure(figsize=(50,50))
heat = sns.heatmap(matrix,annot_kws={"size":80}, fmt='.2f', square=True, cmap='Blues', annot=True)  
heat.set_xlabel("true label",fontsize=80)
heat.set_ylabel("pred label",fontsize=80)
plt.savefig(FILE+"/Matrix.svg")

# Store the trained model
torch.save(model_ft,FILE+"/model")