## Morphometrics and machine learning discrimination of the middle Eocene radiolarian species Podocyrtis chalara, P. goetheana and their morphological intermediates

We provide in this repo the code for the experiments presented in the paper `Morphometrics and machine learning discrimination of the middle Eocene radiolarian species Podocyrtis chalara, P. goetheana and their morphological intermediates`.



## Getting started

Install the requirements:



```bash
pip install -r requirements
```

## How to use ?

### Dataset
We need to download the dataset from [here](https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/8KBOFP)
### Preprocessing
Once we have donwloaded the dataset, we apply the preprocessing to split it and do data augmentation if needed, using the scripts in folder `Preprocessing`:

```bash
python split_dataset.py [dataset path] 
```
```bash
python data_augmentation.py [new splitted dataset path] [augmentation value for each class]
```
### VGG16 model
In folder `VGG16`, train the model by running the next line:
```bash
python VGG16.py [dataset path] [cuda/cpu]
```
### SuperSpike-based
In folder `SuperSpike-based`, train the model using Norse simulator:
```bash
python SuperSpike_model.py [dataset path] [cuda/cpu]
```
### STDP-based
Run the following commands inside the STDP-based folder:

```bash
mkdir build
```
```bash
cd build
```
```bash
cmake ../ -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS='-std=c++11'
```
```bash
make
```
Once the binary files are generated, you can start the simulation from the `build` folder:
```bash
./STDP_based
```
