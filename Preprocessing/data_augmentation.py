"""
Script to do data augmentation

usage:
        python data_augmentation.py [dataset path] [augmentation value]
"""

# import libraries
from skimage import transform
from sys import argv
import cv2
import os
import random
import numpy as np

# global parameters
PATH = str(argv[1])
AUG_IMAGE = str(argv[2])

for dir in os.listdir(PATH):
    i = 0
    while len(os.listdir(PATH+"/"+dir)) < int(AUG_IMAGE):
        for image in os.listdir(PATH+"/"+dir):
            if len(os.listdir(PATH+"/"+dir)) < int(AUG_IMAGE):
                # read original image
                im_original = cv2.imread(PATH+"/"+dir+"/"+image, cv2.IMREAD_GRAYSCALE)
                # rotation
                transformed_img = transform.rotate(im_original,angle= random.randint(-15,15))
                if (random.randint(0,1) == 1):
                    # rescale
                    transformed_img = transform.rescale(transformed_img,1+(random.random()*0.3))
                if (random.randint(0,1) == 1):
                    # horizontal flip
                    transformed_img = np.fliplr(transformed_img)
                # resize
                transformed_img = transform.resize(transformed_img,(1000,1000))
                # store new modified image
                cv2.imwrite(PATH+"/"+dir+f'/{image.split(".jpg")[0]}-{i}.jpg', 255*transformed_img)
                i+=1
            else:
                break