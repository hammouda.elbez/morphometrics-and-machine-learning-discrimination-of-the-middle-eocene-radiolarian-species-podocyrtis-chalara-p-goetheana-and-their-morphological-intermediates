"""
Script to split dataset into Train (80%) and Test (20%) folder

usage:
        python split_dataset.py [dataset path]
"""

# import libraries
import torch
import os
from sys import argv
import shutil
from torchvision.datasets import ImageFolder
from torch.utils.data import random_split

# train/test split
PATH = str(argv[1])
datatrain = ImageFolder(PATH)
train, test = random_split(datatrain, [0.8,0.2], generator=torch.Generator().manual_seed(42))

# moving images to Train Test folders
trainPath = PATH+"_splitted"+"/Train"
os.makedirs(trainPath)
testPath = PATH+"_splitted"+"/Test"
os.makedirs(testPath)
for className in datatrain.classes:
    os.mkdir(trainPath+"/"+className)
    os.mkdir(testPath+"/"+className)
for indice in train.indices:
    os.replace(train.dataset.samples[indice][0], trainPath+train.dataset.samples[indice][0].split(PATH)[1])
for indice in test.indices:
    os.replace(test.dataset.samples[indice][0], testPath+test.dataset.samples[indice][0].split(PATH)[1])

# Removing any folders with no images to avoid crashing the script
countTrain = torch.zeros((len(train.dataset.classes)))
countTest = torch.zeros((len(test.dataset.classes)))
for indice in train.indices:
    countTrain[train.dataset.samples[indice][1]] += 1
for indice in test.indices:
    countTest[test.dataset.samples[indice][1]] += 1
toBeRemoved = 0
for image in [k for k, v in train.dataset.class_to_idx.items() if v in (countTrain<1).nonzero()]+[k for k, v in test.dataset.class_to_idx.items() if v in (countTest<1).nonzero()]:
    toBeRemoved += 1
    shutil.rmtree(trainPath+"/"+image)
    shutil.rmtree(testPath+"/"+image)

print("Removed {} folder from your data".format(toBeRemoved))
