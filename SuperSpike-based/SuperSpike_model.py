"""
VGG16 model training using Norse simulator

usage:
        python SuperSpike_model.py [dataset path] [cuda/cpu]
"""

# import libraries
from norse.torch.module.leaky_integrator import LILinearCell
from torchvision.datasets import ImageFolder
from norse.torch.module.lif import LIFCell
from torch.utils.data import DataLoader
from norse.torch import LIFParameters
from norse.torch.module import encode
from torchvision import transforms
import matplotlib.pyplot as plt
from tqdm import tqdm, trange
import seaborn as sns
from sys import argv
import numpy as np
import datetime
import torch

# global parameters
NOW = datetime.datetime.now()
BATCH_SIZE = 2
DEVICE = torch.device(argv[2] if torch.cuda.is_available() else "cpu")

# data transformation
transform =   transforms.Compose([
        transforms.Resize((128, 128)),
        transforms.ToTensor(),
])

# getting Train and Test set from folders
train = ImageFolder(str(argv[1])+"/Train",transform=transform)
test = ImageFolder(str(argv[1])+"/Test",transform=transform)
class_to_idx = train.class_to_idx

# dataloaders
train_loader = DataLoader(
    train, batch_size=BATCH_SIZE, shuffle=True,
    num_workers=4, pin_memory=True
)
test_loader = DataLoader(
    test, batch_size=BATCH_SIZE, shuffle=False,
    num_workers=4, pin_memory=True
)

# Norse Model definition
class Model(torch.nn.Module):
    def __init__(self, encoder, snn, decoder):
        super(Model, self).__init__()
        self.encoder = encoder
        self.snn = snn
        self.decoder = decoder

    def forward(self, x):
        x = self.encoder(x)
        x = self.snn(x)
        log_p_y = self.decoder(x)
        return log_p_y

class ConvNet(torch.nn.Module):
    def __init__(self, num_channels=3, feature_size=128, method="super", alpha=100):
        super(ConvNet, self).__init__()

        self.features = int(((feature_size - 4) / 2 - 4) / 2)

        self.conv1 = torch.nn.Conv2d(num_channels, 64, 7, 1, bias=False)
        self.conv2 = torch.nn.Conv2d(64, 128, 5, 1, bias=False)
        self.fc1 = torch.nn.Linear(128*52*52, 1000, bias=False)
        self.fc2 = torch.nn.Linear(1000, 100, bias=False)
        self.fc3 = torch.nn.Linear(100, 50, bias=False)
        self.lif0 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.25))
        self.lif1 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.25))
        self.lif2 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.25))
        self.lif3 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.25))
        self.lif4 = LIFCell(p=LIFParameters(method=method, alpha=alpha, v_th=0.25))
        self.out = LILinearCell(50, 3, p=LIFParameters(method=method, alpha=alpha, v_th=0.25))

    def forward(self, x):
        seq_length = x.shape[0]
        batch_size = x.shape[1]

        # specify the initial states
        s0 = s1 = s2 = s3 = s4 = so = None

        voltages = torch.zeros(
            seq_length, batch_size, 3, device=x.device, dtype=x.dtype
        )

        for ts in range(seq_length):
            z = self.conv1(x[ts, :])
            z, s0 = self.lif0(z, s0)
            z = torch.nn.functional.max_pool2d(z, 2, 2)
            z = 10 * self.conv2(z)
            z, s1 = self.lif1(z, s1)
            z = torch.nn.functional.max_pool2d(z, 2, 2)
            z = 10 * z.view(-1, z.shape[1]*z.shape[2]*z.shape[3])
            z = self.fc1(z)
            z, s2 = self.lif2(z, s2)
            z = 10 * self.fc2(z)
            z, s3 = self.lif3(z, s3)
            z = 10 * self.fc3(z)
            z, s4 = self.lif4(z, s4)
            v, so = self.out(torch.nn.functional.relu(z), so)
            voltages[ts, :, :] = v
        return voltages

def train(model, device, train_loader, optimizer):
    '''
    Function to train the model

    Parameters:
        model: the Norse model
        device: using GPU or CPU
        train_loader: trainset loader
        optimizer: the used optimizer
    Returns:
        losses: list of all losses per epoch
        mean_loss: mean loss value
    '''

    model.train()
    losses = []

    for (data, target) in tqdm(train_loader, leave=False):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = torch.nn.functional.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())

    mean_loss = np.mean(losses)
    return losses, mean_loss

def test(model, device, test_loader):
    '''
    Function to test the model

    Parameters:
        model: the Norse model
        device: using GPU or CPU
        test_loader: testset loader
    Returns:
        test_loss: loss value for testset
        accuracy: model accuracy
        matrix: confusion matrix
    '''

    matrix = np.zeros((len(class_to_idx),len(class_to_idx)))
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += torch.nn.functional.nll_loss(
                output, target, reduction="sum"
            ).item()  # sum up batch loss
            pred = output.argmax(
                dim=1, keepdim=True
            )  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

            for x,y in zip(pred,target):
                matrix[x][y] = matrix[x][y] + 1

    test_loss /= len(test_loader.dataset)

    accuracy = 100.0 * correct / len(test_loader.dataset)

    return test_loss, accuracy, matrix

def decode(x):
    """ Function to decode the output of the network using Softmax

    Args:
        x: network output

    Returns:
        classification
    """
    x, _ = torch.max(x, 0)
    log_p_y = torch.nn.functional.log_softmax(x, dim=1)
    return log_p_y

T = 35
LR = 1e-6
EPOCHS = 20  # Increase this for improved accuracy

if torch.cuda.is_available():
    DEVICE = torch.device("cuda")
else:
    DEVICE = torch.device("cpu")

# model definition
model = Model(
    encoder=encode.SpikeLatencyLIFEncoder(T, p=LIFParameters(v_th=0.25)), snn=ConvNet(alpha=80), decoder=decode).to(DEVICE)

#define optimizer
optimizer = torch.optim.Adam(model.parameters(), lr=LR)

file = open("Norse"+str(NOW), 'w+')
training_losses = []
mean_losses = []
test_losses = []
accuracies = []

for epoch in trange(EPOCHS): 
    print(f"Epoch {epoch}")
    training_loss, mean_loss = train(
        model, DEVICE, train_loader, optimizer, epoch, max_epochs=EPOCHS
    )
    test_loss, accuracy, matrix = test(model, DEVICE, test_loader, epoch)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    accuracies.append(accuracy)
    print(f"\nAccuracy: {accuracy},test_loss = {test_loss}")
    file.write("Epoch: "+str(epoch)+"\n")
    file.write("Accuracy: "+str(accuracy)+",test_loss = "+str(test_loss)+"\n")

print(f"final accuracy: {accuracies[-1]}")
file.write("Final accuracy: "+str(accuracies[-1])+"\n")
file.write("time:"+str(datetime.datetime.now() - NOW)+"\n")
file.close()

# Store confusion matrix
plt.figure(figsize=(50,50))
heat = sns.heatmap(matrix,annot_kws={"size":80}, fmt='.2f', square=True, cmap='Blues', annot=True)  
heat.set_xlabel("true label",fontsize=80)
heat.set_ylabel("pred label",fontsize=80)
plt.savefig("Matrix"+"_"+str(NOW)+".svg")

# Store the trained model
torch.save(model,"Norse_model"+"_"+str(NOW))