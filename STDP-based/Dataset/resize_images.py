import os
import sys
from PIL import Image

f = sys.argv[1]
size = int(sys.argv[2])
for dir in os.listdir(f):
    for file in os. listdir(f+"/"+dir):
        f_img = f+"/"+dir+"/"+file
        img = Image.open(f_img)
        img = img.resize((size,size))
        img.save(f_img)
