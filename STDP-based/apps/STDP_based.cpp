#include "Experiment.h"
#include "dataset/Podocyrtis.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "stdp/Proportional.h"
#include "layer/Convolution.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "layer/Pooling.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "stdp/Simplified.h"
#include "stdp/BiologicalMultiplicative.h"
#include "analysis/SaveOutput.h"

int main(int argc, char** argv) {

	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "STDP_Based");

	experiment.add_preprocessing<process::DefaultOnOffFilter>(4, 1.0, 2.0);

	experiment.add_preprocessing<process::FeatureScaling>();
	experiment.input<LatencyCoding>();

	const char* input_path_ptr = "../Dataset";

	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	std::string input_path(input_path_ptr);

	experiment.add_train<dataset::Podocyrtis>(input_path+"/Train/");
	experiment.add_test<dataset::Podocyrtis>(input_path+"/Test/");

	float t_obj_conv1 = 0.80f;
	float t_obj_conv2 = 0.80f;
	float t_obj_fc1 = 0.80f;
	float lr = 1.0f;
	float w_lr = 0.1f;

	auto& conv1 = experiment.push_layer<layer::Convolution>("conv1", 5, 5, 256, 1, 1, 5/2, 5/2);
	conv1.parameter<float>("annealing").set(0.95f);
	conv1.parameter<float>("min_th").set(1.0f);
	conv1.parameter<float>("t_obj").set(t_obj_conv1);
	conv1.parameter<float>("lr_th").set(lr);
	conv1.parameter<Tensor<float>>("w").distribution<distribution::Gaussian>(0.8, 0.01);
	conv1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 1.0);
	conv1.parameter<STDP>("stdp").set<stdp::Multiplicative>(w_lr, 0.0);

	experiment.add_train_step(conv1, 100);

	auto& conv1_out = experiment.output<TimeObjectiveOutput>(conv1, t_obj_conv1);
	conv1_out.add_postprocessing<process::SumPooling>(1, 1);
	conv1_out.add_postprocessing<process::FeatureScaling>();
	conv1_out.add_analysis<analysis::Svm>();

	experiment.run(900);
}
