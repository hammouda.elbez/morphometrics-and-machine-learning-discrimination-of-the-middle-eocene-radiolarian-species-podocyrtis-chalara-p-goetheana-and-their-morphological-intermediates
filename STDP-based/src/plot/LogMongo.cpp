#include "plot/LogMongo.h"
#include "Experiment.h"
#include "Layer.h"
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/array.hpp>
#include <string>

using bsoncxx::builder::basic::document;
using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;

mongocxx::instance inst{};											// This should be done only once.
mongocxx::client conn{mongocxx::uri{"mongodb://127.0.0.1:27017/"}}; // App:timo@
// Add variables for data handling

// get data and time
time_t t = ::time(nullptr);
tm *local_t = ::localtime(&t);
auto t_global = std::chrono::high_resolution_clock ::now();

std::stringstream ss;
mongocxx::database db;

LogMongo LOG = LogMongo();
std::string dataset = "-1";
AbstractExperiment *experiment;

std::vector<bsoncxx::document::value> spikes;
std::vector<bsoncxx::document::value> labels;
std::vector<bsoncxx::document::value> potentials;
std::vector<bsoncxx::document::value> ExistingPotentials;
std::vector<bsoncxx::document::value> synapseWeights;
std::vector<Layer *> layers;
size_t current_layer = 0;
int current_input;
int total_input;
Tensor<float> actualWeight;

// Collections

mongocxx::collection info_collection = db["info"];
mongocxx::collection label_collection = db["labels"];
mongocxx::collection spikes_collection = db["spikes"];
mongocxx::collection potential_collection = db["potential"];
mongocxx::collection synapseWeight_collection = db["synapseWeight"];

LogMongo::LogMongo()
{
	ss << (local_t->tm_mday) << "-" << (local_t->tm_mon) + 1 << "-" << (local_t->tm_year + 1900) << "-" << (local_t->tm_hour) << ":" << (local_t->tm_min) << ":" << (local_t->tm_sec);

	db = conn["csnn-" + ss.str()];
}

void LogMongo::init(std::string d, AbstractExperiment &exp)
{
	dataset = d;
	experiment = &exp;
	store_info();
}

std::string LogMongo::get_dataset_name()
{
	return dataset;
}

void LogMongo::store_info()
{
	auto builder = document{};
	// info to add
	bsoncxx::document::value info_values = make_document(
		kvp("n", experiment->name()),
		kvp("T", ss.str()),
		kvp("D", dataset),
		kvp("L:N", make_document(kvp("Input", static_cast<int>(experiment->input_shape().product())))));

	// getting data as a view to be able to use and store
	bsoncxx::document::view doc_view = info_values.view();

	// insert into databse
	info_collection.insert_one(doc_view);
}

void LogMongo::store_svm(float acc)
{
	auto builder = document{};
	// info to add
	bsoncxx::document::value info_values = make_document(kvp("Acc", acc));

	// getting data as a view to be able to use and store
	bsoncxx::document::view doc_view = info_values.view();

	// insert into databse
	info_collection.insert_one(doc_view);
}

void LogMongo::layers_info(std::vector<Layer *> ls)
{
	// Collect layers information

	layers = ls;
	document info_values;

	for (size_t i = 0; i < layers.size(); i++)
	{
		info_values.append(kvp("L:N." + layers[i]->name(), static_cast<int>(layers[i]->depth())));
	}

	info_collection.update_one(make_document(kvp("D", dataset)), make_document(kvp("$set", info_values)));
}

void LogMongo::update_current_layer(size_t c_layer)
{
	current_layer = c_layer;
	ExistingPotentials.clear();
}

void LogMongo::update_current_input(const std::string c_input)
{
	current_input = stoi(c_input);
	total_input += 1;
	grab_labels();
}

void LogMongo::grab_spikes(size_t neuron_id, std::chrono::_V2::system_clock::time_point tt)
{
	auto t_now = std::chrono::duration_cast<std::chrono::microseconds>(tt - t_global);

	spikes.emplace_back(make_document(
		kvp("T", (t_now.count() / 1000000.0)), // ceil((double)t_now.count() * 1000000.0) / 1000000.0),
		kvp("i", make_document(kvp("L", experiment->layer_at(current_layer).name()), kvp("N", (int)neuron_id))),
		kvp("Input", current_input)));

	if (spikes.size() > 10000)
	{
		store_spikes();
	}
}

void LogMongo::store_spikes()
{
	spikes_collection.insert_many(spikes);
	spikes.clear();
}

void LogMongo::grab_potential(size_t neuron_id, std::chrono::_V2::system_clock::time_point tt, Tensor<float> pot)
{
	auto t_now = std::chrono::duration_cast<std::chrono::microseconds>(tt - t_global);

	double interval = 250;
	if(fmod((t_now.count() / 1000000.0),interval) < 50){
	//std::cout << t_now.count() << std::endl;
	for(size_t z=0; z<neuron_id; z++) {
	//	if (!std::count(ExistingPotentials.begin(), ExistingPotentials.end(),make_document(kvp("L", experiment->layer_at(current_layer).name()),kvp("N", (int)z),kvp("V", ceil((double)pot.at(0,0,z) * 1000000.0) / 1000000.0))))
		//{
		potentials.emplace_back(make_document(
			kvp("T", (t_now.count() / 1000000.0)),
			kvp("L", experiment->layer_at(current_layer).name()),
			kvp("N", (int)z),
			kvp("V", ceil((double)pot.at(0,0,z) * 1000000.0) / 1000000.0)));
		//auto it = find(ExistingPotentials.begin(), ExistingPotentials.end(), make_document(
		//	kvp("L", experiment->layer_at(current_layer).name()),
		//	kvp("N", (int)z),
		//	kvp("V", ceil((double)pot.at(0,0,z) * 1000000.0) / 1000000.0)));
		//if (it != ExistingPotentials.end()) 
    	//{	
		//	ExistingPotentials.at(std::distance(ExistingPotentials.begin(), it)) = make_document(
		//	kvp("L", experiment->layer_at(current_layer).name()),
		//	kvp("N", (int)z),
		//	kvp("V", ceil((double)pot.at(0,0,z) * 1000000.0) / 1000000.0));
//
		//}else{
		//	ExistingPotentials.emplace_back(make_document(
		//	kvp("L", experiment->layer_at(current_layer).name()),
		//	kvp("N", (int)z),
		//	kvp("V", ceil((double)pot.at(0,0,z) * 1000000.0) / 1000000.0)));}
		//}
	
	}}
	if (potentials.size() > 10000)
	{
		store_potential();
	}
}

void LogMongo::store_potential()
{
	potential_collection.insert_many(potentials);
	potentials.clear();
}

void LogMongo::grab_synapseWeight(size_t neuron_id, std::vector<std::vector<u_long>> updatedWeights, std::chrono::_V2::system_clock::time_point tt, Tensor<float> w)
{
	auto t_now = std::chrono::duration_cast<std::chrono::microseconds>(tt - t_global);
	
		for( size_t i=0; i<updatedWeights.size(); i++){
				synapseWeights.emplace_back(make_document(
					kvp("T", (t_now.count() / 1000000.0)),
					kvp("C", (int)updatedWeights[i][2]), // source
					kvp("To", (int)neuron_id), // destination
					kvp("V", w.at(updatedWeights[i][0], updatedWeights[i][1], updatedWeights[i][2], (int)neuron_id)),
					kvp("L", experiment->layer_at(current_layer).name()),
					kvp("index", make_document(kvp("x", (int)updatedWeights[i][0]), kvp("y", (int)updatedWeights[i][1])))));
		}

	if (synapseWeights.size() > 10000)
	{
		store_synapseWeight();
	}
}

void LogMongo::store_synapseWeight()
{
	synapseWeight_collection.insert_many(synapseWeights);
	synapseWeights.clear();
}

void LogMongo::grab_labels()
{
	auto t_now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock ::now() - t_global);
	
	labels.emplace_back(make_document(
		kvp("T", (t_now.count() / 1000000.0)),
		kvp("L", current_input),
		kvp("G", total_input)));

	if (labels.size() > 1000)
	{
		store_labels();
	}
}

void LogMongo::store_labels()
{
	label_collection.insert_many(labels);
	labels.clear();
}

void LogMongo::finalize()
{
	// store the remaining data
	if (spikes.size() > 0)
	{
		store_spikes();
	}
	if (potentials.size() > 0)
	{
		store_potential();
	}
	if (synapseWeights.size() > 0)
	{
		store_synapseWeight();
	}
	if (labels.size() > 0)
	{
		store_labels();
	}
}