#include "dataset/Podocyrtis.h"
#include "iostream"

using namespace dataset;


/*Podocyrtis::Podocyrtis(const std::string& dir) :
	_dir(dir), _cursor(0), _shape({PODOCYRTIS_WIDTH, PODOCYRTIS_HEIGHT, PODOCYRTIS_DEPTH}), _data() {
	QDir dirQ(dir.c_str());
	QStringList learn_images = dirQ.entryList(QStringList() << "*.jpg" << "*.JPG" << "*.bmp", QDir::Files);
	for(int i=0; i<learn_images.count();  i++) {
		_read(QString("%1/%2").arg(dirQ.absolutePath()).arg(learn_images.at(i)), "unk");
	}

}*/

Podocyrtis::Podocyrtis(const std::string& dir) :
	_dir(dir), _cursor(0), _shape({PODOCYRTIS_WIDTH, PODOCYRTIS_HEIGHT, PODOCYRTIS_DEPTH}), _data()  {
	
	QDir dirQ(dir.c_str());
	QStringList folders = dirQ.entryList(QStringList(), QDir::Dirs);

	int x = folders.count();

	for(int i=2; i<x;  i++) {
	QDir dirQ1((dir+folders.at(i).toStdString()).c_str());
	QStringList images = dirQ1.entryList(QStringList() << "*.jpg" << "*.JPG" << "*.bmp", QDir::Files);
	for(int y=0; y<images.count();  y++) {
		_read(QString("%1/%2").arg(dirQ1.absolutePath()).arg(images.at(y)), std::to_string(i-1));
	}
	}
	
	/*QDir dir2(motor_dir.c_str());
	QStringList motor_images = dir2.entryList(QStringList() << "*.jpg" << "*.JPG" << "*.bmp", QDir::Files);
	for(int i=0; i<motor_images.count();  i++) {
		_read(QString("%1/%2").arg(dir2.absolutePath()).arg(motor_images.at(i)), "2");
	}*/
}

bool Podocyrtis::has_next() const {
	return _cursor < _data.size();
}

std::pair<std::string, Tensor<InputType>> Podocyrtis::next() {
	return _data.at(_cursor++);
}

void Podocyrtis::reset() {
	_cursor = 0;
}

void Podocyrtis::close() {
	_data.clear();
}


size_t Podocyrtis::size() const {
	return _data.size();
}
std::string Podocyrtis::to_string() const {
		return "Podocyrtis(learn_set: "+_dir+")";
}

const Shape& Podocyrtis::shape() const {
	return _shape;
}

void Podocyrtis::_read(const QString& filename, const std::string& label) {
	QImage image(filename);
	image = image.scaled(PODOCYRTIS_WIDTH, PODOCYRTIS_HEIGHT, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	_data.emplace_back(label, _shape);

	for(int x=0; x<PODOCYRTIS_WIDTH; x++) {
		for(int y=0; y<PODOCYRTIS_HEIGHT; y++) {
			QRgb pixel = image.pixel(x, y);
			_data.back().second.at(x, y, 0) = static_cast<InputType>(qBlue(pixel));
		}
	}
}
