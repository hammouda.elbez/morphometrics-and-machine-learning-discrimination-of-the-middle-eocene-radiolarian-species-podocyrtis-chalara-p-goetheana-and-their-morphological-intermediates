#include "stdp/Simplified.h"

using namespace stdp;

static RegisterClassParameter<Simplified, STDPFactory> _register("Simplified");

Simplified::Simplified() : STDP(_register), _alpha_p(), _alpha_n(), _beta_p(), _beta_n() {
	add_parameter("alpha_p", _alpha_p);
	add_parameter("alpha_n", _alpha_n);
	add_parameter("beta_p", _beta_p);
	add_parameter("beta_n", _beta_n);
}

Simplified::Simplified(float alpha_p, float alpha_n, float beta_p, float beta_n) : Simplified() {
	parameter<float>("alpha_p").set(alpha_p);
	parameter<float>("alpha_n").set(alpha_n);
	parameter<float>("beta_p").set(beta_p);
	parameter<float>("beta_n").set(beta_n);

}

float Simplified::process(float w, const Time pre, Time post) {
	float v = pre <= post ? w+ _alpha_p * exp(- _beta_p * ((w - 0.0001f)/(1.0f-0.0001f))) :  w- _alpha_n * exp(- _beta_n * ((1.0f-w)/(1.0f-0.0001f)));
	return std::max<float>(0, std::min<float>(1, v));
}

void Simplified::adapt_parameters(float factor) {
	_alpha_p *= factor;
	_alpha_n *= factor;
	_beta_p *= factor;
	_beta_n *= factor;
}
