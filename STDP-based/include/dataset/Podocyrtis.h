#ifndef _DATASET_PODOCYRTIS_H
#define _DATASET_PODOCYRTIS_H

#include <string>
#include <cassert>
#include <fstream>
#include <limits>
#include <tuple>

#include <QDir>
#include <QImage>

#include "Tensor.h"
#include "Input.h"

#define PODOCYRTIS_WIDTH 224
#define PODOCYRTIS_HEIGHT 224
#define PODOCYRTIS_DEPTH 1

namespace dataset {

	class Podocyrtis : public Input {

	public:
		//Podocyrtis(const std::string& learn_dir);
		Podocyrtis(const std::string& dir);

		virtual bool has_next() const;
		virtual std::pair<std::string, Tensor<InputType>> next();
		virtual void reset();
		virtual void close();


		size_t size() const;
		virtual std::string to_string() const;

		virtual const Shape& shape() const;

	private:
		void _read(const QString& filename, const std::string& label);

		std::string _dir;

		uint32_t _cursor;

		Shape _shape;

		std::vector<std::pair<std::string, Tensor<InputType>>> _data;
	};

}

#endif
