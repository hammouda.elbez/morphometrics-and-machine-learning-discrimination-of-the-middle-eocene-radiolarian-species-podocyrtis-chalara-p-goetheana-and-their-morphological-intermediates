#ifndef _LOG_MONGO_H
#define _LOG_MONGO_H

#include <iostream>
#include "Tensor.h"
#include "Spike.h"
#include "time.h"
#include "Layer.h"
#include "layer/Convolution_P.h"
#include "Monitor.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

class LogMongo{

		public:
			LogMongo();
			void init(std::string dataset, AbstractExperiment& experiment);
			void grab_labels();
			void grab_spikes(size_t neuron_id, std::chrono::_V2::system_clock::time_point tt);
			void grab_potential(size_t neuron_id, std::chrono::_V2::system_clock::time_point tt, Tensor<float> pot);
			void grab_synapseWeight(size_t neuron_id, std::vector<std::vector<u_long>> updatedWeights, std::chrono::_V2::system_clock::time_point tt, Tensor<float> w);
			void layers_info(std::vector<Layer *> layer);
			std::string get_dataset_name();
			void update_current_layer(size_t c_layer);
			void store_svm(float acc);
			void update_current_input(const std::string c_input);
			void finalize();
		private:
			static mongocxx::instance inst;
			void store_info();
			void store_labels();
			void store_spikes();
			void store_potential();
			void store_synapseWeight();
			std::string dataset = "-1";
	};

extern LogMongo LOG;


#endif
