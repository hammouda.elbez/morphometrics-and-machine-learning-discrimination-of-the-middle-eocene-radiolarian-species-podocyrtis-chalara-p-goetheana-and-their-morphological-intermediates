#ifndef _EXECUTION_DENSE_LAYER_BY_LAYER_H
#define _EXECUTION_DENSE_LAYER_BY_LAYER_H

#include "Experiment.h"
#include "SpikeConverter.h"

class DenseLayerByLayer {

public:
	typedef Experiment<DenseLayerByLayer> ExperimentType;

	DenseLayerByLayer(ExperimentType& experiment);

	void process(size_t refresh_interval);

	Tensor<Time> compute_time_at(size_t i) const;
private:
	void _prepare(size_t layer_target_index);
	void _process_sample(const std::string& label, const Tensor<Time>& in, size_t layer_index);
	void _update_data(size_t layer_index, size_t refresh_interval);
	void _generate_spike(const Tensor<Time>& in, std::vector<Spike>& out);

	void _load_data();
	void _process_train_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data);
	void _process_test_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data);

	ExperimentType& _experiment;


	std::vector<std::pair<std::string, Tensor<Time>>> _train_set;
	std::vector<std::pair<std::string, Tensor<Time>>> _test_set;

	std::vector<Spike> _current_input;
	std::vector<Spike> _current_output;

	size_t _rc_width;
	size_t _rc_height;
	size_t _rc_depth;
	size_t _current_layer;

};

#endif
