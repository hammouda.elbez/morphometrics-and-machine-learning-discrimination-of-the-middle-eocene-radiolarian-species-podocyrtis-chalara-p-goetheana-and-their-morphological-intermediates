#ifndef _EXECUTION_CHUNK_LAYER_BY_LAYER_H
#define _EXECUTION_CHUNK_LAYER_BY_LAYER_H

#include "Experiment.h"
#include "SpikeConverter.h"
#include "TensorReader.h"
#include <cstdio>

class ChunkLayerByLayer {

public:
	typedef Experiment<ChunkLayerByLayer> ExperimentType;

	ChunkLayerByLayer(ExperimentType& experiment, const std::string& tmp_dir);

	void process(size_t refresh_interval);
	Tensor<Time> compute_time_at(size_t i) const;

private:
	void _prepare(size_t layer_target_index);
	void _process_sample(const std::string& label, const Tensor<Time>& in, size_t layer_index);
	std::pair<size_t, size_t> _update_data(size_t layer_index, size_t refresh_interval, size_t train_id, size_t test_id);
	void _generate_spike(const Tensor<Time>& in, std::vector<Spike>& out);

	std::pair<size_t, size_t> _load_data();
	size_t _process_train_data(Process* process, size_t in_id);
	size_t _process_test_data(Process* process, size_t in_id);

	size_t _create_file();
	void _switch_file(size_t id);
	void _reset_file(size_t id);
	void _delete_file(size_t id);


	bool _has_next(size_t id);
	std::pair<std::string, Tensor<float>> _load(size_t id);

	void _save(size_t id, const std::string& label, const Tensor<float>& tensor);

	ExperimentType& _experiment;

	std::vector<Spike> _current_input;
	std::vector<Spike> _current_output;

	size_t _rc_width;
	size_t _rc_height;
	size_t _rc_depth;
	size_t _current_layer;

	std::string _tmp_dir;

	size_t _file_id;
	std::map<size_t, TensorReader> _readers;
	std::map<size_t, TensorWriter> _writers;

};

#endif
